---
title: "Seismic activity, Upper silesia, 2014-2018"
output:
  html_document:
    toc: true
    theme: united
author: "Bazyli Zoń"
---
# SETUP

## Libraries
```{r message=FALSE}
library(ggthemes)
library(hrbrthemes)
library(tidyverse); theme_set(theme_ipsum_tw())
library(leaflet)
library(ggridges)
library(factoextra)
library(ggpubr)
library(kableExtra)
```
## Loading data
```{r message=FALSE}
df <- read_delim('data.csv', delim = ';')

colnames(df) <- c('Date', 'Time', 'Mag', 'Lat', 'Long')
```

## Data manipulation

getting year, month and day columns for easy access later
```{r}
df <- df %>% 
  mutate(Year = format(as.Date(Date, format="%T-%m-%d"),"%Y")) %>% 
  mutate(Month = format(as.Date(Date, format="%T-%m-%d"),"%m")) %>% 
  mutate(Day = format(as.Date(Date, format="%T-%m-%d"),"%d"))
```


# Clustering

using k-means clustering to find clusters of seismic activity

```{r warning=FALSE}
set.seed(420) # setting seed for reporoducable results
```
Finding optimal number of clusters
```{r warning=FALSE}
fviz_nbclust(df[4:5], kmeans, method = "gap_stat")
```

Performing clustering. Using 7 instead of proposed above 6 clusters since I find it more accurate
```{r warning=FALSE}
km.out <- kmeans(df[4:5], centers = 7, nstart = 100, algorithm = "Lloyd", iter.max = 20)
```

Adding clustering results to data frame
```{r warning=FALSE}
df <- df %>% 
  mutate(cluster = as.factor(km.out$cluster))
```

# DATA


All of this results in column added to dataframe that specifies a cluster where quacke occured
```{r echo=FALSE, results='asis'}
df %>%
  head(10) %>% 
  kable() %>% 
  kable_styling(bootstrap_options = "striped")
```


# MAP

Adding cluster numbers alone doesn give us much information. Maps below halps understanding which cluster refers to what number and how i looks ploted onto map

## heatmap
Heatmap showing a density of quakes
```{r warning=FALSE}
df %>% 
  ggplot(aes(x=Long, y=Lat)) +
  geom_bin2d(bins = 50) +
  scale_fill_continuous(type = "viridis") +
  theme_bw()
```


## leaflet map
Setting up label for leaflet map popup and colour palette
```{r}
label <- paste(
  "Data: ", df$Date, "<br/>",
  "Godz: ", df$Time, "<br/>",
  "Nr zbioru: ", df$cluster, "</br>",
  "Magnituda: ", df$Mag, sep = "") %>% lapply(htmltools::HTML)

pal <- colorFactor(palette = "magma" , domain = c('1','2','3','4','5','6', '7'))

```

Plotting map, where radius of every marker indicate it's strength  and colour, cluster it belongs to

```{r fig.height=7, fig.width=10}
leaflet(df) %>% 
  addTiles() %>% 
  addProviderTiles("OpenStreetMap.Mapnik") %>%
  addCircleMarkers(~Long, ~Lat,
                   fillColor = pal(df$cluster), 
                   fillOpacity = 0.5, 
                   stroke = FALSE,
                   radius = ~Mag^2.3, # so difference in Mag is visible 
                   label = label)
```

# EDA

Looking at some basic info about dataset



```{r results= "asis"}
df %>% 
  group_by(cluster) %>% 
  summarise(mean(Mag), median(Mag), min(Mag), max(Mag)) %>% 
  kable() %>% 
  kable_styling(bootstrap_options = "striped")
```

```{r fig.width=10}
df %>% 
  group_by(cluster) %>% 
  ggplot(aes(x=Mag, y = cluster, fill = cluster)) +
    geom_density_ridges() +
    theme_ridges() + 
    theme(legend.position = "none") +
    scale_fill_viridis_d(option="magma")
```

number of observations in every cluster
```{r warning=FALSE, fig.width=10}
ggplot(df, aes(fct_rev(cluster))) + 
  geom_bar(colour = FALSE)+
  coord_flip()
```


## Date

```{r warning=FALSE, fig.width=10}
ggplot(df, aes(Date, fill = cluster)) +
  geom_histogram(binwidth = 27)
```


```{r warning=FALSE}
df %>% 
  ggplot(aes(Date, group = cluster, fill = cluster)) +
  geom_density(position="fill") +
  theme_ipsum() +
  scale_fill_viridis_d()
```


```{r fig.width=10, warning=FALSE}
yearPlot <- df %>% 
  ggplot(aes(x=Year, fill = cluster)) +
  geom_bar() 



monthPlot <- df %>% 
  ggplot(aes(Month, fill = cluster)) +
    geom_bar() 



dayPlot <- df %>% 
  ggplot(aes(fct_rev(Day), fill = cluster)) +
  geom_bar() +
  coord_flip()  
  
ggarrange(ggarrange(yearPlot, monthPlot, ncol=1, nrow=2), dayPlot, ncol = 2, nrow = 1) 
```


## Exploring Magnitude
```{r fig.width=10, warning=FALSE}
ggplot(df, aes(Mag)) +
  geom_density()
```

```{r fig.width=10}
df %>% 
  group_by(Month) %>% 
  ggplot(aes(x=Mag, y=Month, fill=Month)) +
    geom_density_ridges() +
    theme_ridges() + 
    theme(legend.position = "none")
```





                   